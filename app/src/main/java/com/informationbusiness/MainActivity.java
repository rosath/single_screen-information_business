package com.informationbusiness;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.graphics.Typeface;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String routeFont = "fonts/the_flower_city.ttf";
        String routeFontAwesome = "fonts/fontawesome_webfont.ttf";

        Typeface font = Typeface.createFromAsset(getAssets(), routeFont);
        Typeface fontAwesome = Typeface.createFromAsset(getAssets(), routeFontAwesome);

        TextView textViewName = (TextView) findViewById(R.id.name);
        TextView textViewSlogan = (TextView) findViewById(R.id.slogan);
        TextView textViewAwesomeDirection = (TextView) findViewById(R.id.icon_direction);
        TextView textViewAwesomePhone = (TextView) findViewById(R.id.icon_phone);
        TextView textViewAwesomeEmail = (TextView) findViewById(R.id.icon_email);
        TextView textViewAwesomeWeb = (TextView) findViewById(R.id.icon_web);
        TextView textViewAwesomeCalendar = (TextView) findViewById(R.id.icon_calendar);

        textViewName.setTypeface(font);
        textViewSlogan.setTypeface(font);
        textViewAwesomeDirection.setTypeface(fontAwesome);
        textViewAwesomePhone.setTypeface(fontAwesome);
        textViewAwesomeEmail.setTypeface(fontAwesome);
        textViewAwesomeWeb.setTypeface(fontAwesome);
        textViewAwesomeCalendar.setTypeface(fontAwesome);
    }
}
